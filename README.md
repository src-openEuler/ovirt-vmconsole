# ovirt-vmconsole

#### 介绍
ovirt-vmconsole支持安全访问虚拟机控制台。 它使用SSH协议将控制台从客户传送到目标主机。

它提供了两个组件：

* ovirt-vmconsole-host

    在主机端运行的ssh守护程序实现可信任连接以访问控制台。 假定控制台为Unix域直接连接到qemu虚拟串行的套接字。

* ovirt-vmconsole-proxy

    在最终用户可访问的主机（用户）上运行的ssh守护程序实现，根据从管理器获取的授权控制台的公用密钥访问代理，选择与既定主机连接。

    ovirt-vmconsole软件包不能直接使用，它需要自定义获取用户的授权密钥和用户的授权控制台。
    
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
