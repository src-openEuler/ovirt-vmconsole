%global package_version 1.0.9-1

%global selinuxtype targeted
%global moduletype contrib
%global modulename ovirt_vmconsole

%global default_python_ver 3

%if 0%{?default_python_ver} > 2
%global with_python3 1
%global __python %{__python3}
%global with_python2 0
%else
%global with_python3 0
%global with_python2 1
%endif


Summary:	oVirt VM console
Name:		ovirt-vmconsole
Version:	1.0.9
Release:	2
License:	GPLv3
URL:		http://www.ovirt.org
Source:		http://resources.ovirt.org/pub/src/ovirt-vmconsole-1.0.9-1.tar.gz

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:	checkpolicy
BuildRequires:	gcc
BuildRequires:	gettext
BuildRequires:	openssh-clients
BuildRequires:	openssh-server
BuildRequires:	selinux-policy-devel
BuildRequires:  selinux-policy
BuildArch:	noarch
%{?selinux_requires}

%if 0%{?with_python2}
BuildRequires:	python2-devel
Requires:	python
%endif
%if 0%{?with_python3}
BuildRequires:	python3-devel
Requires:	python3
%endif

Requires(post):		libselinux-utils
Requires(preun):	libselinux-utils
Requires(postun):	libselinux-utils

Requires(pre):		shadow-utils
Requires(preun):	policycoreutils
%if 0%{?with_python2}
Requires(post):		policycoreutils-python
Requires(postun):	policycoreutils-python
%endif
%if 0%{?with_python3}
Requires(post):		policycoreutils-python3
Requires(postun):	policycoreutils-python3
%endif

%description
oVirt VM console proxy

%package host
Summary:	oVirt VM console host components

Requires:	%{name} = %{version}-%{release}
Requires:	openssh-server

Requires(post):		libselinux-utils
Requires(preun):	libselinux-utils

%if 0%{?with_python2}
Requires(post):		policycoreutils-python
Requires(postun):	policycoreutils-python
%endif
%if 0%{?with_python3}
Requires(post):		policycoreutils-python3
Requires(postun):	policycoreutils-python3
%endif

BuildRequires:		systemd
Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

%description host
oVirt VM console host components

%package proxy
Summary:	oVirt VM console proxy components

Requires:	%{name} = %{version}-%{release}
Requires:	openssh-clients

Requires(post):		libselinux-utils
Requires(preun):	libselinux-utils

%if 0%{?with_python2}
Requires(post):		policycoreutils-python
Requires(postun):	policycoreutils-python
%endif
%if 0%{?with_python3}
Requires(post):		policycoreutils-python3
Requires(postun):	policycoreutils-python3
%endif

BuildRequires:		systemd
Requires(post):		systemd
Requires(preun):	systemd
Requires(postun):	systemd

%description proxy
oVirt VM console proxy components

%pre
getent group %{name} >/dev/null || \
	groupadd -r %{name}
getent passwd %{name} >/dev/null || \
	useradd \
		-r \
		-g %{name} \
		-c "oVirt VM Console" \
		-s /bin/sh \
		-d %{_datadir}/%{name}/empty \
		%{name}
%selinux_relabel_pre -s %{selinuxtype}

# [policy-priority]
# keep priority (-P) consistent between install and uninstall to avoid silent failure for removal on %postun
# 400 is just the old default, no reason to change it.
%post
%selinux_modules_install -s %{selinuxtype} -p 400 %{_datadir}/selinux/packages/ovirt-vmconsole/%{modulename}.pp

%postun
if [ $1 -eq 0 ]; then
    # see [policy-priority]
    %selinux_modules_uninstall -s %{selinuxtype} -p 400 %{modulename}
    # remove vmconsole user if uninstall
    userdel %{name} || true
fi

%posttrans
%selinux_relabel_post -s %{selinuxtype}

%post host
if %{_sbindir}/selinuxenabled ; then
	semanage port -a -t ovirt_vmconsole_host_port_t -p tcp 2223 &> /dev/null || :
fi
%systemd_post ovirt-vmconsole-host-sshd.service

# see [ignore-errors]
%postun host
if %{_sbindir}/selinuxenabled ; then
	semanage port -d -p tcp 2223 &> /dev/null || :
fi
%systemd_postun ovirt-vmconsole-host-sshd.service

# see [ordering]
%posttrans host
%selinux_relabel_post -s %{selinuxtype}

%preun host
%systemd_preun ovirt-vmconsole-host-sshd.service

# see [ignore-errors]
%post proxy
if %{_sbindir}/selinuxenabled ; then
	semanage port -a -t ovirt_vmconsole_proxy_port_t -p tcp 2222 &> /dev/null || :
fi
%systemd_post ovirt-vmconsole-proxy-sshd.service

# see [ignore-errors]
%postun proxy
if %{_sbindir}/selinuxenabled ; then
	semanage port -d -p tcp 2222 &> /dev/null || :
fi
%systemd_postun ovirt-vmconsole-proxy-sshd.service

# see [ordering]
%posttrans proxy
%selinux_relabel_post -s %{selinuxtype}

%preun proxy
%systemd_preun ovirt-vmconsole-proxy-sshd.service

%prep
%setup -q -n %{name}-%{package_version}

%build
%configure \
	--with-local-version="%{name}-%{version}-%{release}" \
	%{?conf}
make %{?_smp_mflags}
make -f /usr/share/selinux/devel/Makefile -C selinux-ovirt_vmconsole

%install
rm -rf "%{buildroot}"
make %{?_smp_mflags} install DESTDIR="%{buildroot}"
# install policy modules
install -d %{buildroot}%{_datadir}/selinux/packages/ovirt-vmconsole
install -m 0644 selinux-ovirt_vmconsole/%{modulename}.pp %{buildroot}%{_datadir}/selinux/packages/ovirt-vmconsole

#
# workaround rpmlint warnings
#
find "%{buildroot}" -name .keep -exec rm {} \;

#
# Register services
#
install -dm 755 "%{buildroot}%{_unitdir}"
install -m 644 "src/ovirt-vmconsole-host/ovirt-vmconsole-host-sshd/ovirt-vmconsole-host-sshd.systemd" "%{buildroot}%{_unitdir}/ovirt-vmconsole-host-sshd.service"
install -m 644 "src/ovirt-vmconsole-proxy/ovirt-vmconsole-proxy-sshd/ovirt-vmconsole-proxy-sshd.systemd" "%{buildroot}%{_unitdir}/ovirt-vmconsole-proxy-sshd.service"

%files
%dir %{_datadir}/%{name}
%dir %{_sysconfdir}/%{name}
%dir %{_datadir}/%{name}/empty
%if 0%{?with_python2}
%dir %{python_sitelib}/ovirt_vmconsole
%{python_sitelib}/ovirt_vmconsole/__init__.py*
%{python_sitelib}/ovirt_vmconsole/common/
%endif
%if 0%{?with_python3}
%dir %{python3_sitelib}/ovirt_vmconsole
%{python3_sitelib}/ovirt_vmconsole/__init__.py*
%{python3_sitelib}/ovirt_vmconsole/common/
%exclude %{python3_sitelib}/ovirt_vmconsole/__pycache__/*
%endif
%{_datadir}/selinux/packages/ovirt-vmconsole/%{modulename}.pp
%{_docdir}/%{name}/
%{_sysconfdir}/pki/%{name}/

%files host
%{_datadir}/%{name}/ovirt-vmconsole-host/
%{_libexecdir}/ovirt-vmconsole-host-*
%{_sysconfdir}/%{name}/ovirt-vmconsole-host/
%{python_sitelib}/ovirt_vmconsole/ovirt_vmconsole_host_*/
%{_unitdir}/ovirt-vmconsole-host-sshd.service

%files proxy
%{_datadir}/%{name}/ovirt-vmconsole-proxy/
%{_libexecdir}/ovirt-vmconsole-proxy-*
%{_sysconfdir}/%{name}/ovirt-vmconsole-proxy/
%{python_sitelib}/ovirt_vmconsole/ovirt_vmconsole_proxy_*/
%{_unitdir}/ovirt-vmconsole-proxy-sshd.service

%changelog
* Wed Mar 16 2022 wangdi <wangdi@kylinos.cn> - 1.0.9-2
- remove user ovirt-vmconsole after removing package

* Tue Jul 6 2021 chenjian <chenjian@kylinos.cn> - 1.0.9-1
- update to 1.0.9-1

* Thu Mar 5 2020 di.wang <di.wang@cs2c.com.cn> - 1.0.7-2
- Package Initialization
