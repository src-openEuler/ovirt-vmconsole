# ovirt-vmconsole

#### Description

ovirt-vmconsole enables secure access to virtual machine serial console. It uses SSH protocol to tunnel the console from customer to destination host.

Two components are available:

* ovirt-vmconsole-host

    ssh daemon implementation that runs on the host end enables trusted connections to access the consoles. Consoles are assumed to be unix domain sockets that are directly attached to qemu virtual serial.

* ovirt-vmconsole-proxy

    ssh daemon implementation that runs on the end user accessible host, users access the proxy, based on their public key the authorized consoles are fetch from a manager, once selected a connection to the host is established.

    The ovirt-vmconsole package cannot be used as-is, it requires customization to fetch users' authorized keys and users' authorized consoles.

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
